/*
* @author Mike Lawson
*/

const app = require("../conf/app");
/*{
    contentRoutes: require("../conf/content-routes"),
    dataRoutes: require("../conf/data-routes"),
    appConfig: require("../conf/app"),
};*/

app.use("/", require("../conf/content-routes"));

module.exports = app;