/*
* @author Mike Lawson
*/

// boot the chat application into a global that we can use across the chat object
const app = require("../bin/bootstrap");

// let's roll
import CafeChat from "../bin/cafe-chat";

CafeChat.startChatting();

module.exports = app;