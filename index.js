
// es6 hooks
require("@babel/register")({
    presets: ["@babel/preset-env"]
});
require("babel-polyfill");
const app = require("./bin/index");
const http = require("http");

app.set('port', 9000);

const server = http.createServer(app);

server.listen(9000);
