/*
* @author Mike Lawson
*/
/*eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }]*/
const express = require("express");
const path = require("path");
const router = express.Router();

router.get("/help", (req, res) => {
    res.sendFile(path.join(__dirname + '/../public/help.html'));
});


module.exports = router;