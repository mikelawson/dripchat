FORMAT: 1A

# DripChat Client

The Dripchat client API serves to represent interactions between a consuming
client and the chat server. Items to note include

- Messages are all signed
- Both messages and metadata are encrypted
- WebSockets must be supported by the client interface
- DripChat is entirely asynchronous by nature, always
- WIP

# Group Handshake

## Work with a chat session [/chat]

### Initialize a session [POST]

+ Request (application/json)
    
    {
        "clientDisplayName":"some client name",
        "timestamp":"MM/DD/YYYY 00:00:00 UTC+x",
        "clientKeyRequestNonce":"some encoded nonce value",
        "apiKey":"encrypted api key",
        "chatDestination":"account name or company name",
        "encryptedSessionToken":"shows user is logged in",
        "encryptedHostUserId":"exactly what it says"
           
     }
        
+ Response 201 (application/json)
    
    + Headers
            
        Cache-Control: no-cache, no-store, must-revalidate
        Accept-Language: us-EN
        Accept-Charset: utf-8
                
    + Body
        
        {
            "message" : "connection established",
            "connId" : "connection identifier"    
        }
        
+ Response 403 (application/json)

    + Headers
            
        Cache-Control: no-cache, no-store, must-revalidate
        Accept-Language: us-EN
        Accept-Charset: utf-8
               
     + Body
     
        {
            "message" : "unauthorized request"
        }
        
+ Response 503 (application/json)

    + Headers
    
        Cache-Control: no-cache, no-store, must-revalidate
        Accept-Language: us-EN
        Accept-Charset: utf-8
    
    + Body
    
        {
            "message" : "user is not available"
        }
        
        
### Close a session [DELETE]

+ Request (application/json)
    
    {
        "timestamp":"MM/DD/YYYY 00:00:00 UTC+x",
        "clientKeyRequestNonce":"some encoded nonce value",
        "apiKey":"encrypted api key",
        "chatDestination":"account name or company name",
        "connId" : "connectionId"
           
     }
        
+ Response 200 (application/json)
    
    + Headers
            
        Cache-Control: no-cache, no-store, must-revalidate
        Accept-Language: us-EN
        Accept-Charset: utf-8
                
    + Body
        
        {
            "message" : "connection closed"    
        }
        
+ Response 403 (application/json)

    + Headers
            
        Cache-Control: no-cache, no-store, must-revalidate
        Accept-Language: us-EN
        Accept-Charset: utf-8
               
     + Body
     
        {
            "message" : "unauthorized request"
        }
        
+ Response 500 (application/json)

    + Headers
    
        Cache-Control: no-cache, no-store, must-revalidate
        Accept-Language: us-EN
        Accept-Charset: utf-8
    
    + Body
    
        {
            "message" : "error while ending connection, disconnect forced"
        }
        
  
# Group Client Setup

## Session Options [/options]

### Get a JSON object of options for this instance [OPTION]

       
+ Response 201 (application/json)
    
    + Headers
            
        Cache-Control: no-cache, no-store, must-revalidate
        Accept-Language: us-EN
        Accept-Charset: utf-8
                
    + Body
        
        [
            {
                option1: {
                    key: "some key",
                    value: "some value"
                }
            }
        ]

### Get a specific option [GET]

+ Request

    + Parameters
    
        + option_name (string, required)
        
+ Response 200 (application/json)

    + Body
    
        {
            key: "some key",
            value: "some value",
            type: "some datatype"
        }

+ Response 403 (application/json)

    + Body
    
        {
            "message": "option not available for use"
        }                

        
+ Response 404 (application/json)

    + Body
    
        {
            "message": "no option found by that name"
        }                